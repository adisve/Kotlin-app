package com.example.fortesting.Utilites

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.fortesting.activities.ControlActivity

class WakeupReceiver : BroadcastReceiver() {
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onReceive(context: Context?, intent: Intent?) {
        ControlActivity().writeCharacteristic()
        Log.i("Confirmation", "Message sent")
    }
}