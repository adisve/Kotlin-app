package com.example.fortesting.activities
import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.widget.AdapterView
import com.example.fortesting.Utilites.MyListAdapter
import com.example.fortesting.databinding.ActivityMainBinding
import java.util.*
import kotlin.collections.ArrayList

private const val LOCATION_PERMISSION_REQUEST_CODE = 2
private const val ENABLE_BLUETOOTH_REQUEST_CODE = 1

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class MainActivity : AppCompatActivity() {
    private val deviceNames: ArrayList<String> = ArrayList()
    private lateinit var binding: ActivityMainBinding

    private val bleScanner by lazy {
        mBluetoothAdapter.bluetoothLeScanner
    }
    private val mBluetoothAdapter: BluetoothAdapter by lazy {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private val scanSettings = ScanSettings.Builder()
        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
        .build()

    private val isLocationPermissionGranted
        get() = hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)

    private fun Context.hasPermission(permissionType: String) : Boolean{
        return ContextCompat.checkSelfPermission(this, permissionType) ==
                PackageManager.PERMISSION_GRANTED
    }

    private var isScanning = false
        set(value){
            field = value
            runOnUiThread { binding.selectDeviceRefresh.text = if(value) "STOP"
            else "SEARCH"}
        }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.selectDeviceRefresh.setOnClickListener{
            if(isScanning) {
                stopBleScan()
                isScanning = false
            }
            else startBleScan()
        }
        binding.selectDeviceRefresh.setOnClickListener{ if (isScanning) stopBleScan()
        else startBleScan() }
    }

    private fun stopBleScan(){
        bleScanner.stopScan(scanCallback)
        isScanning = false
    }

    private fun getDevice(position: Int): BluetoothDevice?{
        return deviceList[position].device
    }

    private val scanCallback = object : ScanCallback() {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            val index = deviceList.indexOfFirst {
                it.device.address == result.device.address
            }

            if (index != -1) {
                deviceList[index] = result

            }else if (result.device.name != null){
                with(result.device){
                    Log.i("ScanCallback", "Found BLE device. Name: ${result.device.name},"
                            + " address: $address")
                    deviceList.add(result)
                    deviceNames.add(result.device.name)
                }
            }
            val adapter = MyListAdapter(this@MainActivity, deviceNames)
            binding.selectDeviceList.adapter = adapter
            binding.selectDeviceList.onItemClickListener =

                AdapterView.OnItemClickListener{ _, _, position, _ ->
                    if(isScanning) {
                        stopBleScan()
                        isScanning = false
                    }
                    try {
                        getDevice(position).let { device ->
                            val intent = Intent(
                                this@MainActivity, ControlActivity::class.java
                            )
                            intent.putExtra(ControlActivity.EXTRAS_DEVICE_ADDRESS, device!!.address)
                            intent.putExtra(ControlActivity.EXTRAS_DEVICE_NAME, device.name)
                            startActivity(intent)
                        }
                    }catch (e: NullPointerException) {
                        e.printStackTrace()
                    }
                }
        }
        override fun onScanFailed(errorCode: Int) {
            Log.i("ScanCallback", "onScanFailed: code $errorCode")
        }
    }
    // Second thing that happens
    override fun onResume() {
        super.onResume()
        if(!mBluetoothAdapter.isEnabled){
            promptEnableBluetooth()
        }
    }

    // Initiates bluetooth scan for devices
    @RequiresApi(Build.VERSION_CODES.M)
    private fun startBleScan()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !isLocationPermissionGranted){
            requestLocationPermission()
        }else{
            deviceList.clear()
            bleScanner.startScan(null, scanSettings, scanCallback)
            isScanning = true
        }
    }
    private fun requestLocationPermission()
    {
        if(isLocationPermissionGranted) return
        val alert = AlertDialog.Builder(this)
        runOnUiThread{
            alert.apply{
                alert.setTitle("Location permission required")
                alert.setMessage("Starting from Android M (6.0), the system requires app to be granted "
                        + "location access in order to scan for BLE devices")
                alert.setCancelable(false)
                alert.setPositiveButton("OK"
                ) { _, _ ->
                    requestPermission(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        LOCATION_PERMISSION_REQUEST_CODE
                    )
                }
                alert.setNegativeButton("CANCEL"
                ){_, _ ->
                    finish()
                }
            }.show()
        }
    }

    // Requests permission for device/user location
    private fun Activity.requestPermission(permission: String, requestCode: Int){
        ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if(grantResults.firstOrNull() == PackageManager.PERMISSION_DENIED){
                    requestLocationPermission()
                } else startBleScan()
            }
        }
    }

    private fun promptEnableBluetooth()
    {
        if(!mBluetoothAdapter.isEnabled){
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, ENABLE_BLUETOOTH_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            ENABLE_BLUETOOTH_REQUEST_CODE ->{
                if(resultCode != Activity.RESULT_OK) promptEnableBluetooth()
            }
        }
    }

    companion object{
        val deviceList = mutableListOf<ScanResult>()
        const val EXTRA_ADDRESS: String = "Device_address"
        const val EXTRA_NAME: String = "Device_name"
    }
}