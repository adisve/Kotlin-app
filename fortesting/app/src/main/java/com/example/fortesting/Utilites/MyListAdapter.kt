package com.example.fortesting.Utilites

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.fortesting.R
import java.util.ArrayList


class MyListAdapter(private val context: Activity, private val deviceNames: ArrayList<String>)
    : ArrayAdapter<String>(context, R.layout.custom_list, deviceNames)  {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.custom_list, null, true)

        val deviceText = rowView.findViewById(R.id.device_name) as TextView

        deviceText.text = deviceNames[position]

        return rowView
    }
}