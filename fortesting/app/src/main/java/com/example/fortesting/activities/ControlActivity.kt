package com.example.fortesting.activities

import android.annotation.SuppressLint
import android.app.*
import android.bluetooth.*
import android.content.*
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.fortesting.Utilites.WakeupReceiver
import com.example.fortesting.databinding.ControlLayoutBinding
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import java.util.*

class ControlActivity: AppCompatActivity(){
    private lateinit var binding: ControlLayoutBinding
    private lateinit var picker: MaterialTimePicker
    private lateinit var wakeupManager : AlarmManager
    private lateinit var pendingIntent : PendingIntent
    private var mDeviceAddress: String? = null
    private var mDeviceName: String? = null

    private val mBluetoothAdapter: BluetoothAdapter by lazy {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ControlLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        createNotificationChannel()

        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS).toString()
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME).toString()

        binding.sendButton.setOnClickListener{writeCharacteristic()}
        binding.controlWakeupButton.setOnClickListener { showAlarmSetter()}
        binding.controlCancelWakeup.setOnClickListener { cancelWakeup() }
        binding.controlLedDisconnect.setOnClickListener { disconnect() }
    }

    override fun onResume() {
        super.onResume()
        val device = mBluetoothAdapter.getRemoteDevice(mDeviceAddress)
        device.connectGatt(this, false, mGattCallback)
    }

    fun writeCharacteristic(){
        if(mBluetoothGatt == null){
            Log.w(TAG, "BluetoothGatt not initialized")
            return
        }
        val service = mBluetoothGatt!!.getService(
            UUID.fromString("0000FFE0-0000-1000-8000-00805f9b34fb"))
        val characteristic : BluetoothGattCharacteristic =
            service.getCharacteristic(UUID.fromString(
                "0000FFE1-0000-1000-8000-00805f9b34fb"))
        mBluetoothGatt?.let { gatt ->
            characteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE
            characteristic.value = "1".toByteArray()
            gatt.writeCharacteristic(characteristic)
        } ?: error("No established connection")
    }


    @SuppressLint("UnspecifiedImmutableFlag")
    private fun cancelWakeup(){
        wakeupManager = getSystemService(ALARM_SERVICE) as AlarmManager
        val intent  = Intent(this, WakeupReceiver::class.java)
        pendingIntent = PendingIntent.getBroadcast(
            this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        wakeupManager.cancel(pendingIntent)
        Log.d("cancel:", "cancelled alarm")
    }


    @SuppressLint("UnspecifiedImmutableFlag")
    private fun setWakeUp(){
        wakeupManager = getSystemService(ALARM_SERVICE) as AlarmManager
        val intent  = Intent(this, WakeupReceiver::class.java)
        pendingIntent = PendingIntent.getBroadcast(
            this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val newHour:Int = picker.hour
        val newMinute:Int = picker.minute
        val calendar: Calendar = Calendar.getInstance().apply{
            set(Calendar.HOUR_OF_DAY, newHour)
            set(Calendar.MINUTE, newMinute)
        }

        wakeupManager.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY,
            pendingIntent
        )
        Toast.makeText(this, String.format("Alarm set for %02d:%02d", newHour, newMinute),
            Toast.LENGTH_SHORT).show()
    }

    // Time Picker for setting wakeup
    private fun showAlarmSetter(){
        picker = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .setHour(12)
            .setMinute(0)
            .setTitleText("Pillow wakeup")
            .build()

        picker.show(supportFragmentManager, "Aurora tech")

        picker.addOnPositiveButtonClickListener {
            setWakeUp()
        }
    }

    private fun disconnect(){
        mBluetoothGatt!!.disconnect()
        mBluetoothGatt!!.close()
        finish()
    }

    private fun createNotificationChannel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name : CharSequence = "auroraNotifChannel"
            val description = "Channel for wakeup call"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel("aurora", name, importance)
            channel.description = description

            val notificationManager = getSystemService(
                Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.createNotificationChannel(channel)
        }
    }

    private val mGattCallback = object : BluetoothGattCallback(){
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            val intentAction: String
            if(newState == BluetoothProfile.STATE_CONNECTED){
                Log.i("BluetoothGatt", "Successfully connected to device")
                Log.i(TAG, "Connected to GATT server.")
                mBluetoothGatt = gatt
                /* Attempts to discover services after successful connection. */
                Log.i(
                    TAG, "Attempting to start service discovery:")
                Handler(Looper.getMainLooper()).post {
                    mBluetoothGatt?.discoverServices()
                }

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "Disconnected from GATT server.")
                gatt.close()
            }

        }
        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                with(gatt){
                    Log.w("BluetoothGattCallback", "Discovered ${services.size}" +
                            " services for ${device.address}")
                    printGattTable()
                }
            } else {
                Log.w(TAG, "onServicesDiscovered received: $status")
            }
        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            with(characteristic) {
                when (status) {
                    BluetoothGatt.GATT_SUCCESS -> {
                        Log.i("BluetoothGattCallback",
                            "Wrote to characteristic $uuid | value: $value")
                    }
                    BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH -> {
                        Log.e("BluetoothGattCallback",
                            "Write exceeded connection ATT MTU!")
                    }
                    BluetoothGatt.GATT_WRITE_NOT_PERMITTED -> {
                        Log.e("BluetoothGattCallback",
                            "Write not permitted for $uuid!")
                    }
                    else -> {
                        Log.e("BluetoothGattCallback",
                            "Characteristic write failed for $uuid, error: $status")
                    }
                }
            }
        }
    }
    private fun BluetoothGatt.printGattTable() {
        if (services.isEmpty()) {
            Log.i("printGattTable", "No service and characteristic available, call discoverServices() first?")
            return
        }
        services.forEach { service ->
            val characteristicsTable = service.characteristics.joinToString(
                separator = "\n|--",
                prefix = "|--"
            ) { it.uuid.toString() }
            Log.i("printGattTable", "\nService ${service.uuid}\nCharacteristics:\n$characteristicsTable"
            )
        }
    }

    companion object{
        private var mBluetoothGatt: BluetoothGatt? = null
        @JvmField var EXTRAS_DEVICE_NAME = "DEVICE_NAME"
        @JvmField var EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS"
        private val TAG = ControlActivity::class.java.simpleName
    }
}